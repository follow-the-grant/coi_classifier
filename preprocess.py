import pandas as pd
from Preprocessor import Preprocessor

data = pd.read_csv('./COI_training_data.csv')
data = data.sample(30000)    # sample data to reduce computation cost

print('Preprocess Data')
preprocessor = Preprocessor()

data['tokens'] = data['sentences'].apply(preprocessor.word_tokenize)
data['tokens'] = data['tokens'].apply(preprocessor.process)

print('Build Vocabulary')
vocab, freq = preprocessor.build_vocab(data.tokens.tolist())
preprocessor.store_vocab(vocab)

print('Encode sentences')
data['encoded'] = data['tokens'].apply(lambda x: preprocessor.encode(x, vocab))

data.to_csv('./processed_data.csv', index=False)
