from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.naive_bayes import MultinomialNB
import pandas as pd
import pickle
import ast


data = pd.read_csv('./processed_data.csv')
data['encoded'] = data['encoded'].apply(ast.literal_eval)
data['tokens'] = data['tokens'].apply(ast.literal_eval)

# Split train and test data
train_data = data.sample(frac=0.5, random_state=42)
test_data = data.drop(train_data.index)

trainX = train_data.encoded.to_list()
trainY = train_data.COI.to_list()

testX = test_data.encoded.to_list()
testY = test_data.COI.to_list()

# Run classifier
clf = MultinomialNB()
clf.fit(trainX, trainY)

def save_classifier(classifier):
    with open('model.pkl', 'wb') as f:
        pickle.dump(classifier, f)


save_classifier(clf)

# Evaluation
predictions = clf.predict(testX)

accuracy = accuracy_score(testY, predictions)
confusion_matrix = confusion_matrix(testY, predictions)

print('Accuracy: ', accuracy)
print('Confusion Matrix:\n', confusion_matrix)

FP = []
FN = []
TP = []
TN = []
for idx, pred in enumerate(predictions):
    sentence = test_data.iloc[idx].sentences
    if pred != testY[idx]:
        if pred:
            FP.append((idx, sentence))
        else:
            FN.append((idx, sentence))
    else:
        if pred:
            TP.append((idx, sentence))
        else:
            TN.append((idx, sentence))
