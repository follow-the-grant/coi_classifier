import pandas as pd
import string
from nltk import FreqDist
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


STOPWORDS = stopwords.words('english')
STOPWORDS = [word for word in STOPWORDS if word != 'no']

punctuation = set(string.punctuation)
EXCLUDE = STOPWORDS + list(punctuation)

wordnet_lemmatizer = WordNetLemmatizer()


class Preprocessor:

    def __init__(self, n_vocab=2000):
        self.n_vocab = n_vocab
        self.tokenizer = Tokenizer()

    def word_tokenize(self, sentence):
        return word_tokenize(sentence)

    def sentence_tokenize(self, text):
        return self.tokenizer.tokenize(text)

    def process(self, tokens, exclude=EXCLUDE, lemmatizer=wordnet_lemmatizer):
        words = []
        for word in tokens:
            if word not in exclude:
                lemma = lemmatizer.lemmatize(word.lower(), pos='v')
                words.append(lemmatizer.lemmatize(lemma))
        return words

    def encode(self, tokens, vocab):
        res = [0] * (len(vocab) + 1)
        for token in tokens:
            try:
                # count appearance of words in vocab
                idx = vocab.index(token)
                res[idx] += 1
            except ValueError:
                # count out of vocab words
                res[-1] += 1
        return res

    def build_vocab(self, list_of_tokens):
        all_tokens = []
        for tokens in list_of_tokens:
            all_tokens += tokens
        freq_dist = FreqDist(all_tokens)
        most_common_words = freq_dist.most_common()[:self.n_vocab]
        return zip(*most_common_words)

    def store_vocab(self, vocab, path='./vocab.txt'):
        with open(path, 'w') as f:
            for word in vocab:
                f.write(word + '\n')

    def load_vocab(self, path='./vocab.txt'):
        with open(path, 'r') as f:
            vocab = f.read()
        return vocab.split('\n')[:-1]


class Tokenizer:

    def __init__(self):
        punkt_param = PunktParameters()
        abbreviations = ['drs', 'dr', 'prof', 'ltd', 'mr', 'ms', 'inc', 'u.s',
                         'co', 'n', 'syn', 'sp', 'comb', 'm']
        punkt_param.abbrev_types = set(abbreviations)
        self.tokenizer = PunktSentenceTokenizer(punkt_param)

    def tokenize(self, text):
        return self.tokenizer.tokenize(text)
