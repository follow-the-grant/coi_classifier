import argparse
from Preprocessor import Preprocessor
import pickle


def load_classifier():
    with open('model.pkl', 'rb') as f:
        classifier = pickle.load(f)
    return classifier


def predict(classifier, preprocessor, text):
    vocab = preprocessor.load_vocab()
    predictions = []
    encoded_sentences = []
    sentences = preprocessor.sentence_tokenize(text)
    for sentence in sentences:
        tokens = preprocessor.word_tokenize(sentence)
        if len(tokens) >= 3:
            tokens = preprocessor.process(tokens)
            encoded_sentences.append(preprocessor.encode(tokens, vocab))

    results = classifier.predict(encoded_sentences)
    return zip(results, sentences)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('path', type=str)
    parser.add_argument('--text', action='store_true')

    arg = parser.parse_args()
    if arg.text:
        text = arg.path
    else:
        with open(arg.path, 'r') as f:
            text = f.read()

    preprocessor = Preprocessor()
    classifier = load_classifier()
    results = predict(classifier, preprocessor, text)
    print('COI related sentences:')
    for prediction, sentence in results:
        if prediction:
            print(sentence)
