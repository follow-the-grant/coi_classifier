from Preprocessor import Tokenizer
import pandas as pd
import random

'''
This scrips builds a (preliminary) database for training a classifier to
identify conflict of interest statements

- negative sample sentences (sentences that have nothing to do with a statement about a
conflict of interest) are randomly sampled from the abstract of already
processed articles

- positive sample sentences (sentences that make a statement about the conflict
of interest of authors (could be positive or negative)) are taken from
previsously extracte COI statements

'''


def filter_sentence_length(sentences, min_n=3, log_short=False):
    valid_sentences = []
    for sentence in sentences:
        if len(sentence.split()) >= min_n:
            valid_sentences.append(sentence)
        else:
            if log_short:
                print(sentence)
    return valid_sentences


# Load tokenizer
tokenizer = Tokenizer()

# positive samples
COI_PATH = '../data/cois_corrected.csv'
coi_data = pd.read_csv(COI_PATH, sep=',', low_memory=False)
coi_data.dropna(axis=0, subset=['coi_text'], inplace=True)

coi_data = coi_data[coi_data.coi_text != 'NONE']

print('Number of articles with COI statement: {}'.format(len(coi_data)))
print('Number of unique COI statement: {}'.format(len(coi_data.coi_text.unique())))
print('Most common sentences:')
print(coi_data.coi_text.value_counts()[:15])

# For a first proof of concept only the unique COI statements are used. A
# better sampled set might be added at a later stage
coi_samples = coi_data.coi_text.unique().tolist()

# Split COI statements in sentences to make their length and format comparable
# to negative samples
positive_samples = []
for coi_statement in coi_samples:
    for sentence in tokenizer.tokenize(coi_statement):
        if len(sentence.split()) >= 3:
            positive_samples.append(sentence)

positive_df = pd.DataFrame(positive_samples, columns=['sentences'])
positive_df.drop_duplicates(inplace=True)
positive_df['COI'] = 1


# negative samples
ARTICLE_PATH = '../data/articles.csv'
article_data = pd.read_csv(ARTICLE_PATH, sep='\t')
# Sample data to reduce computation time as nltk sentence tokenizer is costly
article_data = article_data.sample(500000)

article_data.dropna(axis=0, subset=['abstract'], inplace=True)

articles = article_data['abstract'].tolist()
print('Number of articles: {}'.format(len(articles)))

full_article_texts = ' '.join(articles)
all_sentences = tokenizer.tokenize(full_article_texts)
all_sentences = filter_sentence_length(all_sentences)

# sample the sentences in order to balance the number of positive and negative
# training data
negative_samples = random.sample(all_sentences, len(positive_samples))

print('Number of sampled positive sentences: {}'.format(len(negative_samples)))
print('Number of sampled negative sentences: {}'.format(len(negative_samples)))

negative_df = pd.DataFrame(negative_samples, columns=['sentences'])
negative_df['COI'] = 0

# Join samples
full_samples = pd.concat([positive_df, negative_df])
full_samples.to_csv('./COI_training_data.csv', index=False)
